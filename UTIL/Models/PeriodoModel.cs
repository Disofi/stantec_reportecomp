﻿namespace UTIL.Models
{
    public class PeriodoModel
    {
        public int id { get; set; }
        public string mes { get; set; }
        public string ano { get; set; }
    }
}

