﻿using DocumentFormat.OpenXml.Presentation;
using DsDashBoard.UTIL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UTIL;
using UTIL.Models;

namespace DsDashBoard.Controllers
{
    public class ReporteController : BaseController
    {
        [HttpGet]
        public ActionResult ReporteRRHH()
        {
            try
            {
               

                List<VariableModel> variables= controlDisofi().getVariables(baseDatosUsuario());
                ViewBag.variables = variables;
                List<VariableModel> variablesReporte = controlDisofi().getVariablesReporte(baseDatosUsuario());
                ViewBag.variablesReporte = variablesReporte;
                List<PeriodoModel> periodo = controlDisofi().getPeriodo();
                //string  anoP = DateTime.Now.Year.ToString();
                //string nMes = DateTimeFormatInfo.CurrentInfo.GetMonthName(DateTime.Now.Month);

                //var mes = periodo.Where(a=>a.mes==nMes && a.ano==anoP).ToList();
                
                ViewBag.periodo= periodo;
                
                UsuarioModel user = obtenerUsuario();
                ViewBag.tipo = user.IdTipo;

                return View();
                
            }
            catch(Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        //Limpiar Tabla Variables de Reporte
        [HttpPost]
        public JsonResult deleteVariablesReporte(string codVariable)
        {
            try
            {
                var resp = controlDisofi().deleteVariablesReporte(codVariable);

                return Json(new { data = resp });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }
        //Limpiar Tabla Variables de Reporte
        [HttpPost]
        public JsonResult insertVariablesReporte(string codVariable, string descripcion)
        {
            try
            {
                var resp = controlDisofi().insertVariablesReporte(codVariable, descripcion);

                return Json(new { data = resp });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        //Traer las fichas de softland
        [HttpPost]
        public JsonResult getFichas(string estado, string estadoAnterior, string mes, string mesAnterior, string ano)
        {
            LogUser.agregarLog("Estado : "+ estado);
            LogUser.agregarLog("estado Anterior : " + estadoAnterior);
            LogUser.agregarLog("Mes  : " + mes);
            LogUser.agregarLog("mes Anterior: " + mesAnterior);
            LogUser.agregarLog("año :" + ano);
            string mesActualAFP = mes;
            string mesAnteriorAFP = mesAnterior;

            if (ano == "2023")
            {
                switch (mes)
                {
                    case "1":
                        mesActualAFP = "13";
                        break;
                    case "2":
                        mesActualAFP = "14";
                        break;
                    case "3":
                        mesActualAFP = "15";
                        break;
                    case "4":
                        mesActualAFP = "16";
                        break;
                    case "5":
                        mesActualAFP = "17";
                        break;
                    case "6":
                        mesActualAFP = "18";
                        break;
                    case "7":
                        mesActualAFP = "19";
                        break;
                    case "8":
                        mesActualAFP = "20";
                        break;
                    case "9":
                        mesActualAFP = "21";
                        break;
                    case "10":
                        mesActualAFP = "22";
                        break;
                    case "11":
                        mesActualAFP = "23";
                        break;
                    case "12":
                        mesActualAFP = "24";
                        break;
                    default:
                        mesActualAFP = "0";
                        break;
                }
            }
            else
            {
                mesActualAFP = mes;
                mesAnteriorAFP = mesAnterior;
            }
            if (mesActualAFP != "13" && ano == "2023")
            {
                if (ano == "2023")
                {
                    switch (mesAnterior)
                    {
                        case "1":
                            mesAnteriorAFP = "13";
                            break;
                        case "2":
                            mesAnteriorAFP = "14";
                            break;
                        case "3":
                            mesAnteriorAFP = "15";
                            break;
                        case "4":
                            mesAnteriorAFP = "16";
                            break;
                        case "5":
                            mesAnteriorAFP = "17";
                            break;
                        case "6":
                            mesAnteriorAFP = "18";
                            break;
                        case "7":
                            mesAnteriorAFP = "19";
                            break;
                        case "8":
                            mesAnteriorAFP = "20";
                            break;
                        case "9":
                            mesAnteriorAFP = "21";
                            break;
                        case "10":
                            mesAnteriorAFP = "22";
                            break;
                        case "11":
                            mesAnteriorAFP = "23";
                            break;
                        case "12":
                            mesAnteriorAFP = "24";
                            break;
                        default:
                            mesAnteriorAFP = "0";
                            break;
                    }
                }
            }
            else
            {
                mesAnteriorAFP = ano == "2023" ? "12" : mesAnteriorAFP;
            }

           

            try
            {
                List<FichaModel> data = controlDisofi().getFichas(baseDatosUsuario(), estado, estadoAnterior, mes, mesAnterior, ano, mesActualAFP, mesAnteriorAFP);

                return Json(new { data = data });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        //Traer valores de variables de softland
        [HttpPost]
        public JsonResult getValores(string numFicha, string codVariable, string mes, string mesAnterior)
        {
            try
            {

                List<ValorModel> data = controlDisofi().getValores(baseDatosUsuario(), numFicha, codVariable, mes, mesAnterior);

                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].NumFicha.Contains("."))
                    {
                        data[i].NumFicha = data[i].NumFicha.Contains(".") ? data[i].NumFicha.Replace(".", "-") : data[i].NumFicha;
                    }
                    else
                    {
                        data[i].NumFicha = data[i].NumFicha.Contains(",") ? data[i].NumFicha.Replace(",", "-") : data[i].NumFicha;
                    }
                }
                return Json(new { data = data });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }
        
    }

    
}
